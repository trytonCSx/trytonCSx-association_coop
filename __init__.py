# Copyright 2021 Hartmut Goebel <h.goebel@crazy-compilers.com>
# Licensed under the GNU General Public License v3 or later (GPLv3+)
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.pool import Pool
from . import configuration
from . import member
from . import shares

__all__ = ['register']


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationShareBookingSequence,
        configuration.ConfigurationShareAmount,
        configuration.ConfigurationCompulsoryShares,
        member.Member,
        shares.ShareBooking,
        module='association', type_='model')  # TODO: correct module?
