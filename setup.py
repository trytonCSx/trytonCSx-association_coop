#!/usr/bin/env python3
# This file was originally taken from Tryton.
# Licensed under the GNU General Public License v3 or later (GPLv3+)
# SPDX-License-Identifier: GPL-3.0-or-later

import io
import os
import re
from configparser import ConfigParser
from setuptools import setup, find_packages

MODNAME = "association_coop"
name = f'trytonCSx-{MODNAME}'
MODULE2PREFIX = {
    #'association': 'trytonCSx',
}

def read(fname):
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()


def get_require_version(name):
    if minor_version % 2:
        require = '%s >= %s.%s.dev0, < %s.%s'
    else:
        require = '%s >= %s.%s, < %s.%s'
    require %= (name, major_version, minor_version,
        major_version, minor_version + 1)
    return require


config = ConfigParser()
config.read_file(open(os.path.join(os.path.dirname(__file__), 'tryton.cfg')))
info = dict(config.items('tryton'))
for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()
version = info.get('version', '0.0.1')
major_version, minor_version, _ = version.split('.', 2)
major_version = int(major_version)
minor_version = int(minor_version)

requires = []
for dep in info.get('depends', []):
    if not re.match(r'(ir|res)(\W|$)', dep):
        prefix = MODULE2PREFIX.get(dep, 'trytond')
        requires.append(get_require_version('%s_%s' % (prefix, dep)))
requires.append(get_require_version('trytond'))

tests_require = [get_require_version('proteus')]
dependency_links = []
if minor_version % 2:
    dependency_links.append('https://trydevpi.tryton.org/')

setup(name=name,
    version=version,
    description='Tryton module managing members of cooperatives',
    long_description=read('README.rst'),
    author='Tryton',
    author_email='bugs@tryton.org',
    url='http://www.tryton.org/',
    keywords='tryton association',
    package_dir={f'trytond.modules.{MODNAME}': '.'},
    packages=([f'trytond.modules.{MODNAME}'] +
              [f'trytond.modules.{MODNAME}.{p}' for p in find_packages()]),
    package_data={
        f'trytond.modules.{MODNAME}': (info.get('xml', []) + [
            'tryton.cfg', 'view/*.xml', 'locale/*.po', 'icons/*.svg', '*.fodt',
            'tests/*.rst'
        ]),
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Framework :: Tryton',
        'Intended Audience :: Developers',
        'License :: OSI Approved ::'
        'GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Natural Language :: German',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Office/Business',
    ],
    license='GPL-3',
    python_requires='>=3.6',
    install_requires=requires,
    dependency_links=dependency_links,
    zip_safe=False,
    entry_points=f"""
    [trytond.modules]
    {MODNAME} = trytond.modules.{MODNAME}
    """,
    test_suite='tests',
    test_loader='trytond.test_loader:Loader',
    tests_require=tests_require,
)
