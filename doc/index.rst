###########################
Cooparative Association
###########################

This module handles Memberships in Cooperative Associations,
where members subscribe to shares.
So this module adds handling of shares to Members.


Concepts
################

A Party becomes a Member after approval only.


Termination
******************************


Admonishment & Expulsion
******************************


Configuration
####################

Membership Configurations
******************************

:Share Amount: Nominal value of one share.

:Compulsory shares: Minimum number of Cooperative Shares a member has
                    to sign.


.. Emacs config:
 Local Variables:
 mode: rst
 ispell-local-dictionary: "american"
 End:
