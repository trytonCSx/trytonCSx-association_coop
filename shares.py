# Copyright 2021 Hartmut Goebel <h.goebel@crazy-compilers.com>
# Licensed under the GNU General Public License v3 or later (GPLv3+)
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.i18n import gettext
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.model.exceptions import AccessError
from trytond.pool import Pool
from trytond.pyson import Eval, If

__all__ = ['Shareholder', 'ShareBooking']

_BOOKING_STATES = [
    ('draft', "Draft"),
    ('posted', "Posted"),
]

class ShareBooking(Workflow, ModelSQL, ModelView):
    "Shareholder of a Cooperative"
    __name__ = 'association.share.booking'

    _states = {'readonly': Eval('state') != 'draft'}
    _depends = ['state']

    state = fields.Selection(_BOOKING_STATES, "State", readonly=True)
    code = fields.Char("Code", select=True,
        states={
            'readonly': True,
            'required': Eval('state') != 'draft'
            },
        depends=_depends,
        help="The internal identifier for the booking.")
    member = fields.Many2One('association.member', "Member",
        required=True,
        states=_states, depends=_depends,
        help="The member this share booking belongs to.")
    quantity = fields.Integer(
        "Number of Shares", required=True,
        domain=[('quantity', '!=', 0)],
        states=_states, depends=_depends,
        help="Number of shares in this subscription.")
    amount_per_share = fields.Numeric(  # FIXME: Monetary
        "Value per share", required=True,
        readonly=True,  # assume fixed value for all shares for now
        domain=[('amount_per_share', '>', 0)],
        states=_states, depends=_depends,
        help="Nominal value of one share.")
    amount = fields.Function(
        # TODO: Monetary
        fields.Numeric("Amount", help="Nominal value of transaction."),
        'get_amount')
    signature_date = fields.Date("Signature Date", required=True,
        domain=[
            If(Eval('signature_date') & Eval('effective_date'),
               ('signature_date', '<=', Eval('effective_date')),
               ()),
        ],
        states=_states,
        depends=_depends + ['effective_date', 'member.signature_date'],
        help="Date of signature for this subscription.")
    effective_date = fields.Date("Effective Date", required=True,
        # TODO: Does "effective_date" actually make sense?
        domain=[
            If(Eval('effective_date') & Eval('signature_date'),
               ('effective_date', '>=', Eval('signature_date')),
               ()),
            # TODO: validate "effective_date >= member.join_date"
            # If(Eval('effective_date') & Eval('member.join_date'),
            #    ('effective_date', '>=', Eval('member.join_date')),
            #    ()),
        ],
        states=_states,
        depends=_depends + ['signature_date', 'member.effective_date'],
        help="Date the subscription become effective.")
    comment = fields.Char("Comment",
        states=_states, depends=_depends,
        help=("Note here if this is an unusual transaction, "
              "e.g. a transfer between members or "
              "if the termination period is not hold."))  # FIXME: wording

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._transitions |= {('draft', 'posted')}

        cls._buttons.update({
            'post': {
                'pre_validate': [('effective_date', '!=', None),],
                'invisible': Eval('state') != 'draft',
                'depends': ['state'],
            },
        })

    @classmethod
    def _new_code(cls, **pattern):
        Configuration = Pool().get('association.configuration')
        config = Configuration(1)
        sequence = config.get_multivalue('share_booking_sequence', **pattern)
        if sequence:
            return sequence.get()

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_effective_date(cls):
        date = Pool().get('ir.date')
        return date.today()

    @classmethod
    def default_amount_per_share(cls):
        Configuration = Pool().get('association.configuration')
        config = Configuration(1)
        return config.get_multivalue('share_amount')

    @classmethod
    @ModelView.button
    @Workflow.transition('posted')
    def post(cls, bookings):
        for booking in bookings:
            # TODO: Only if member.state != draft
            if not booking.code:
                booking.code = cls._new_code()
        cls.save(bookings)

    def get_amount(self, name):
        return self.quantity * self.amount_per_share

    @classmethod
    def delete(cls, bookings):
        for booking in bookings:
            if booking.state != 'draft':
                raise AccessError(
                    gettext('association_coop.msg_booking_delete_posted',
                            booking=booking.rec_name))
        super().delete(bookings)
