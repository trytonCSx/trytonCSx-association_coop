# Copyright 2021 Hartmut Goebel <h.goebel@crazy-compilers.com>
# Licensed under the GNU General Public License v3 or later (GPLv3+)
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, If

__all__ = ['Member',]

class Member(metaclass=PoolMeta):
    "Member of a Cooperative, subscribed to shares"
    __name__ = "association.member"

    num_shares = fields.Function(
        fields.Integer(
            "Number of Shares", readonly=True,
            depends=["share_bookings"],
            help=("Number of Cooperative Shares the member has subscribed "
                  "as of today.")),
        'get_num_shares')

    shares_value = fields.Function(
        fields.Numeric(  # TODO: Monetary
            "Share Amount",
            depends=["share_bookings"],
            help=("Nominal value of the shares the member has subscribed "
                  "as of today.")),
        'get_shares_value')

    share_bookings = fields.One2Many('association.share.booking', 'member',
        "Share Transactions",
        #domain=[('company', '=', Eval('company', -1))],
        #depends=['company'],
        #states=_states
                                     )

    # TODO: When "admitting" the member, "post" all "draft" bookings?

    def get_num_shares(self, name=None):
        if not self.share_bookings:
            return 0
        today = Pool().get('ir.date').today()
        cnt = sum(
            booking.quantity
            for booking in self.share_bookings
            if booking.state != "draft" and booking.effective_date <= today)
        return cnt

    def get_shares_value(self, name=None):
        if not self.share_bookings:
            return 0
        today = Pool().get('ir.date').today()
        amount = sum(
            booking.amount
            for booking in self.share_bookings
            if booking.state != "draft" and booking.effective_date <= today)
        return amount

    @classmethod
    def view_attributes(cls):
        attributes = super().view_attributes() + [
            ('/tree/field[@name="num_shares"]', 'visual',
             If(Eval('state') == 'stopped',
                If(Eval('num_shares',0) != 0, 'warning', ''),
                If(Eval('num_shares',0) < 2, #  FIXME: get from config
                   'danger', ''))),
            ('/tree', 'visual',
             If(Eval('state') == 'stopped', 'muted',
                If(Eval('state') == 'draft', 'warning',
                   If(Eval('state') == 'admitted', 'warning', '')))),
            ]
        return attributes
