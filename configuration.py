# Copyright 2021 Hartmut Goebel <h.goebel@crazy-compilers.com>
# Licensed under the GNU General Public License v3 or later (GPLv3+)
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.model import fields, ModelSQL
from trytond.pyson import Eval, Id
from trytond.pool import Pool, PoolMeta

from trytond.modules.company.model import CompanyValueMixin
# from trytond.modules.currency.fields import Monetary  TODO


share_booking_sequence = fields.Many2One(
    'ir.sequence', "Share Booking Sequence",
    required=True,
    domain=[
        ('sequence_type', '=', Id('association_coop',
                                  'sequence_type_share_booking')),
    ],
    help=("Sequence for FIXME"))

share_amount = fields.Numeric(  # TODO: Monetary(
    "Share Amount",
    required=True,
    help="Nominal value of one share.")

compulsory_shares = fields.Integer(
    "Compulsory shares",
    required=True,
    help="Minimum number of Cooperative Shares a member has to sign.")


class Configuration(metaclass=PoolMeta):
    "Cooperative Association Configuration"
    __name__ = 'association.configuration'

    share_booking_sequence = fields.MultiValue(share_booking_sequence)
    share_amount = fields.MultiValue(share_amount)
    compulsory_shares = fields.MultiValue(compulsory_shares)

    @classmethod
    def default_compulsory_shares(cls, **pattern):
        return 1

    @classmethod
    def default_share_booking_sequence(cls, **pattern):
        return getattr(
            cls.multivalue_model('share_booking_sequence'),
            'default_share_booking_sequence', lambda: None)()


class ConfigurationShareBookingSequence(ModelSQL, CompanyValueMixin):
    """Share Booking Configuration Sequence"""
    __name__ = 'association.configuration.share_booking_sequence'

    share_booking_sequence = share_booking_sequence

    @classmethod
    def default_share_booking_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('association_coop',
                                    'sequence_share_booking')
        except KeyError:
            return None


class ConfigurationShareAmount(ModelSQL, CompanyValueMixin):
    """Share Amount"""
    __name__ = 'association.configuration.share_amount'
    share_amount = share_amount


class ConfigurationCompulsoryShares(ModelSQL, CompanyValueMixin):
    """Compulsory Shares"""
    __name__ = 'association.configuration.compulsory_shares'
    compulsory_shares = compulsory_shares
